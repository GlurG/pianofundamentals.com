<?php

	error_reporting(E_ALL);
	
	// Directory Constants

	define('PS', PATH_SEPARATOR);
	define('DS', DIRECTORY_SEPARATOR);

	define('DIR_BASE',           realpath(dirname(__FILE__) . '/../') . DS);
	
	define('DIR_LIBRARY',        DIR_BASE . 'library' . DS);
	define('DIR_APPLICATION',    DIR_BASE . 'application' . DS);
	define('DIR_LANGUAGES',      DIR_APPLICATION . 'languages' . DS);
	define('DIR_MODULES',        DIR_APPLICATION . 'modules' . DS);
	define('DIR_DEFAULT_MODULE', DIR_MODULES . 'default' . DS);

	// Include Paths

	set_include_path(
		PS . DIR_LIBRARY . 
		PS . DIR_APPLICATION . 
		PS . get_include_path()
	);

	//require_once DIR_DEFAULT_MODULE . 'misc' . DS . 'create_lucene_index.php';
	
	require_once 'Zend/Loader.php';
	Zend_Loader::registerAutoload();
	
	$config = new Zend_Config_Ini(DIR_APPLICATION . 'config.ini', 'production');
	
	Zend_Layout::startMvc(array(
		'layoutPath' => DIR_DEFAULT_MODULE . 'views' . DS . 'layouts'
	));
	
	// Front Controller Setup
	
	$front = Zend_Controller_Front::getInstance();
	$front->addModuleDirectory(DIR_MODULES);
	$front->throwExceptions($config->ThrowExceptions);
	
	// Router Setup (order must be preserved)
	$router = $front->getRouter();
	
	$router->addRoute('book_lang', new Zend_Controller_Router_Route(
		'book/:lang',
		array(
			'controller' => 'book',
			'action'     => 'view')
		)
	);
	
	// Overrides book/:lang
	$router->addRoute('book_search', new Zend_Controller_Router_Route_Static(
		'book/search',
		array(
			'controller' => 'book',
			'action'     => 'search')
		)
	);
	
	$router->addRoute('book_section', new Zend_Controller_Router_Route(
		'book/:lang/:section',
		array(
			'controller' => 'book',
			'action'     => 'view')
		)
	);
	
	$front->dispatch();
?>