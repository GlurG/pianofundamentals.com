<?php

	class BookSearcher
	{
		private $options;
		private $lang;
		private $index;
		private $errorMsg;
		
		function __construct($options = null, $lang = null)
		{
			$this->setOptions($options);
			$this->setLang($lang);
		}
		
		function setOptions($options)
		{
			$this->options = strToLower($options);
		}
		
		function setLang($lang)
		{
			$this->lang = ($lang == null) ? 'en' : strToLower($lang);
			
			// Change locale (if needed)
			if ($this->lang != 'en') {
				switch ($this->lang) {
					case 'de': $locale = 'de_DE'; break;
					case 'fr': $locale = 'fr_FR'; break;
					default:
						error_log("BookSearcher - Undefined language: {$this->lang}");
						$locale = 'en_US';
						break;
				}

				setlocale(LC_CTYPE, $locale . '.utf-8');
			}
		}
		
		function query($keywords)
		{
			$path = DIR_LANGUAGES . "{$this->lang}/index/";
			
			/*if (!$this->validate($keywords)) {
				return false;
			}*/
			
			require_once 'Zend/Search/Lucene.php';
			
			$keywords = $this->sanitize($keywords);
			$this->openIndex($path);
			
			if (!($query = $this->buildQuery($keywords))) {
				return false;
			}
			
			return $this->index->find($query);
		}
		
		function getErrorMsg()
		{
			return $this->errorMsg;
		}
		
		private function sanitize($keywords)
		{
			// Special words must be quoted
			$search  = array('/\band\b/', '/\bor\b/', '/\bnot\b/', '/\bto\b/');
			$replace = array('"and"', '"or"', '"not"', '"to"');
			
			return preg_replace($search, $replace, $keywords);
		}
		
		private function validate($keywords)
		{
			if (strlen($keywords) < 3) {
				$this->errorMsg = 'Your search query is too short. A minimum of 3 characters is required.';
				
				return false;
			}
			
			return true;
		}
		
		private function openIndex($path)
		{
			try {
				$this->index = Zend_Search_Lucene::open($path);
			}

			// Fatal error - show 500 page
			catch (Zend_Search_Lucene_Exception $e) {
				throw new Zend_Search_Lucene_Exception("Unable to open index for searching: {$e->getMessage()}");
			}
		}
		
		private function buildQuery($keywords)
		{
			switch ($this->options) {
				case 'title':
					$queryString = "title:($keywords)";
					break;

				case 'section':
					$queryString = "body:($keywords)";
					break;

				case 'both':
					$queryString = "title:($keywords) OR body:($keywords)";
					break;

				default:
					error_log("Undefined search option: {$this->options}");
					$queryString = "title:($keywords)";
					break;
			}
			
			try {
				$query = Zend_Search_Lucene_Search_QueryParser::parse($queryString);
			}
			
			catch (Zend_Search_Lucene_Search_QueryParserException $e) {
				error_log("Zend_Search_Lucene_Search_QueryParserException - Syntax error: $queryString | {$e->getMessage()}");
				$this->errorMsg = "Malformed search query. Please use only alphanumeric characters (a-z, 0-9), +, -, \" and ^";
				
				return false;
			}
			
			return $query;
		}
	}
?>