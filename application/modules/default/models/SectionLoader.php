<?php

	require_once 'Section.php';

	class SectionLoader
	{
		private $lang;
		private $filename;
		
		function __construct($lang = null)
		{
			$this->setLang($lang);
		}
		
		function setLang($lang)
		{
			$this->lang = ($lang == null) ? 'en' : strToLower($lang);
		}
		
		function setFilename($filename)
		{
			$this->filename = ($filename == null) ? 'toc' : $filename;
		}
		
		function fetch($filename)
		{
			$this->setFilename($filename);
			
			$path = DIR_LANGUAGES . "{$this->lang}/book/{$this->filename}.xml";
			
			if (!is_readable($path)) {
				return false;
			}
			
			$section = simplexml_load_file($path);
			
			return (($section == false) ? false : new Section($this->filename, $section));
		}
	}
?>