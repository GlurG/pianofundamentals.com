<?php
	
	require_once realpath(dirname(__FILE__) . '/../../../config/directories.php');
	
	require_once 'Zend/Search/Lucene.php';
	
	function isValidSection($fileName)
	{
		return !(
			strstr($fileName, 'abbreviations') ||
			strstr($fileName, 'chapter') ||
			strstr($fileName, 'toc')
		);
	}
	
	$langs = new DirectoryIterator(DIR_LANGUAGES);
	
	foreach ($langs as $lang) {
		if (!$lang->isDir() || $lang->isDot()) {
			continue;
		}
		
		$pathBook  = $lang->getPathname() . '/book';
		$pathIndex = $lang->getPathname() . '/index';
		
		try {
			$index = Zend_Search_Lucene::open($pathIndex);
		}
		
		catch (Zend_Search_Lucene_Exception $e) {
			error_log("Zend_Search_Lucene - Unable to open index for updating: $pathIndex");
			exit;
		}
		
		$files = glob("$pathBook/*.xml");
		sort($files);
		
		foreach ($files as $file) {
			if (isValidSection($file)) {
				$section = simplexml_load_file($file);
				$sectIdx = $index->find($search = 'filename:(' . basename($file, '.xml') . ')');
				
				var_dump($search);
			}
		}
	}
?>